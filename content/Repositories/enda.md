Title: enda
Date: 2023-10-11 17:00
Tags: python, enda, forecast
Slug: enda
Contributor: Enercoop
Project: REScoopVPP
ProjectUrl: rescoopvpp


### Repository location

[https://github.com/enercoop/enda](https://github.com/enercoop/enda)

Published on Pypi: [https://pypi.org/project/enda/](https://pypi.org/project/enda/)

### Description

Python package that provides tools to manipulate timeseries data in conjunction with contracts data for analysis and forecasts. It contains wrappers around ML backends such as H2O and Scikit for forecasting. It provides tools and methods to featurize electricity-market related data, and examples about the way of performing a forecast.  