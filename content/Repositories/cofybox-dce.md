Title: COFYbox DCE
Date: 2023-09-11 11:00
Tags: python, cofybox
Slug: cofybox-dce
Contributor: EnergieID
Project: REScoopVPP
ProjectUrl: rescoopvpp


### Repository location

[https://gitlab.com/rescoopvpp/cofybox-dce](https://gitlab.com/rescoopvpp/cofybox-dce)

Published on Pypi: [https://pypi.org/project/cofybox-dce/](https://pypi.org/project/cofybox-dce/)

### Description

Description: Data Collector Engine. Collects data from external data sources (such as dynamic prices or weather) and outputs them as Pandas DataFrames for further analysis or storage.