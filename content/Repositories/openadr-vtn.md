Title: Open ADR VTN
Date: 2023-09-11 11:00
Tags: python, OpenADR, ADR, DSR, homeassistant, hass
Slug: open-adr-vtn
Contributor: CarbonCoop
Project: REScoopVPP
ProjectUrl: rescoopvpp

### Repository location

[https://gitlab.com/carboncoop/pyoadr-vtn](https://gitlab.com/carboncoop/pyoadr-vtn)

### Description
A Django server which implenents OpenADR (Automated Demand Response), a standard for alerting and responding to the need to adjust electric power consumption in response to fluctuations in grid demand. This element is the virtual top node (VTN) and sends signals to a virtual end node (VEN).