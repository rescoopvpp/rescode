Title: COFYbox DCE Azure Functions
Date: 2023-09-11 11:00
Tags: python, cofybox, azure, azure-functions
Slug: cofybox-dce-azure-functions
Contributor: EnergieID
Project: REScoopVPP
ProjectUrl: rescoopvpp


### Repository location

[https://gitlab.com/rescoopvpp/cofybox-dce-azure-functions](https://gitlab.com/rescoopvpp/cofybox-dce-azure-functions)


### Description

Wrapper to deploy COFYbox DCE Python Code in Microsoft Azure Functions