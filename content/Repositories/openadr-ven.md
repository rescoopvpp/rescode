Title: Open ADR VEN
Date: 2023-09-11 11:00
Tags: python, OpenADR, ADR, DSR, homeassistant, hass
Slug: open-adr-ven
Contributor: CarbonCoop
Project: REScoopVPP
ProjectUrl: rescoopvpp

### Repository location

[https://gitlab.com/carboncoop/pyoadr-ven](https://gitlab.com/carboncoop/pyoadr-ven)

### Description
A Django client which implenents OpenADR (Automated Demand Response), a standard for alerting and responding to the need to adjust electric power consumption in response to fluctuations in grid demand. This element is the virtual end node (VEN) and recieves signals from a virtual top node (VTN).