Title: COFYbox.io
Date: 2023-09-11 11:00
Tags: cofybox, c#, css, html, javascript, typescript
Slug: cofybox.io
Contributor: EnergieID
Project: REScoopVPP
ProjectUrl: rescoopvpp


### Repository location

[https://gitlab.com/rescoopvpp/cofybox.io](https://gitlab.com/rescoopvpp/cofybox.io)

### Documentation location

[https://docs.cofybox.io/en/cofycloud](https://docs.cofybox.io/en/cofycloud)

### Description

Source code of the cofybox.io website