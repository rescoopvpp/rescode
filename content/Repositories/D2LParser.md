Title: D2LParser
Date: 2023-09-11 11:00
Tags: python, cofybox, D2L, EEsmart, France
Slug: d2lparser
Contributor: EnergieID
Project: REScoopVPP
ProjectUrl: rescoopvpp


### Repository location

[https://gitlab.com/rescoopvpp/D2LParser](https://gitlab.com/rescoopvpp/D2LParser)

Published on Pypi: [https://pypi.org/project/D2LParser/](https://pypi.org/project/D2LParser/)

### Description

Python package to decrypt and parse communications coming from the Eesmart D2L-dongle, which is a piece of hardware used to read digital meters in France.