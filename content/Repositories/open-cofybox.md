Title: Open COFYbox
Date: 2023-09-25 11:00
Tags: python, balena, cofybox, docker, open-source
Slug: open-cofybox
Contributors: EnergieID
    CarbonCoop
Project: REScoopVPP
ProjectUrl: rescoopvpp

### Repository location

[https://gitlab.com/rescoopvpp/open-cofybox](https://gitlab.com/rescoopvpp/open-cofybox)

### Description
Open source version of the COFYbox code.