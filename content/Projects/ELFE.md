Title: ELFE
Date: 2021-11-01 11:00
Tags: France
Slug: elfe
Contributor: EPV

<img src="../images/elfe.png" width=200>


The ELFE project (local experimentation of energy flexibility) aims at matching renewable energy production of the Redon territory with local consumption, in a real-time fashion, implementing and experimenting with flexibility. As a test project, no power contract will be modified, all analysis will run on data only.  

EPV has set up an IT infrastructure to collect fine-grained power data (1min step) for both production and consumption, provide a rolling 24h-ahead “power balance forecast”, and then to actively shift consumptions on the best timespots, to maximize energy self-consumption on the area while respecting participant’s will on their flexible assets.

The project has been unrolling in several steps since its start at the end of 2021 :

    1. enrollment of citizens, SMEs and municipalities (representing in total 100 members/buildings) and motivating them into adopting active flexibility practices;  
    2. Procurement, development, installation and operation of the necessary IT infrastructure and IoT devices to control flexible assets (e.g. electrical vehicle, water heater, washing machines, etc.) according to the local power forecast through an Energy Management System (EMS) developed by project partner ENS Rennes (École Nationale Supérieure de Rennes);
    3. development of a web-based user interface that allows members to visualize and understand the local impacts of flexibility on the electricity balance of the area.  

NB : All the IT infrastructure is under free and open-source licence. Some components are off-the-shelf available softwares for data management, some specific code (EMS and Command&Control) has been developed under the EUPL v1.2 licence. All this work is published on GitHub and aims to be shared with other citizen-led flexibility projects, to build common energy playgrounds.

After months of technical installation and code development, the project enters the “exploitation” phase as of september 2023. The next months will be dedicated to the supervision of the flexibility actions, and to collecting and analyzing the data provided by users. 

In parallel to technical operation, a survey has been launched at the beginning of October 2023 to analyze behavior of the participants in relation to the experimentation. The aim of this survey is to understand the ways participants are taking part in the project, but also to identify key factors to replicate successfully such an experimentation at a larger scale (500 participants). This psycho-sociological survey will run until June 2024.
Visit [ELFE's website](https://www.projet-elfe.fr) (in French) for more information.

### Repositories

- [EMS ELFE](/elfe-ems)
- [Commande ELFE](/elfe-commande)
