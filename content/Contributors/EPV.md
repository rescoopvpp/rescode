Title: Energies citoyennnes en Pays de Vilaine
Tags: France
Slug: epv
Date: 2023-10-31 17:00

<img src="../images/epv.png" width="300" />

Énergies citoyennes en Pays de Vilaine is a grass-root organization founded in 2003 by inhabitants of the Pays de Vilaine territory in eastern Brittany (33 municipalities and 66 000 inhabitants). The initial purpose of the founders was to re-appropriate energy issue through developing wind turbines, financed and managed by citizens. 

Since 2003, EPV works with citizens and municipalities for the development of community-based
renewable energy projects. The association is at the origin of the first wind farms 100% developed and financed by citizens (3 plants, 13 wind turbines, 28 MW, +2000 people involved) in France. 

Current activities include support to new local projects initiated by citizens (wind and solar) and operation of the existing plants. EPV also runs projects on energy management (flexibility, demand side management, sustainable transportation, etc.) and climate change. One of the relevant experiences in the team is the project ECCO (creating new local Energy Community Co-Operatives) that EPV implemented as a partner from 2019 to 2021 in the framework of the Interreg NWE.
Visit [our website](https://epv.enr-citoyennes.fr/) for more information.

### Projects

- [ELFE](/elfe)

### Repositories

- [EMS ELFE](/elfe-ems)
- [Commande ELFE](/elfe-commande)
