#!/usr/bin/env python
# -*- coding: utf-8 -*- #

AUTHOR = "REScode"
SITENAME = "REScode"
SITEURL = ""

PATH = "content"

TIMEZONE = "Europe/Brussels"

DEFAULT_LANG = "en"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = None

# Social widget
SOCIAL = None

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

USE_FOLDER_AS_CATEGORY = True

# Theme

THEME = "themes/bootstrap2-modified"

