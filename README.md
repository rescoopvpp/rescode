# REScode on GitLab Pages

The website can be found at [code.rescoop.eu](https://code.rescoop.eu).

## Pelican

This website is build with Pelican, a static site generator written in Python.
See [Pelican's documentation](https://docs.getpelican.com/en/latest/index.html) for more information.

---

## GitLab CI

This project's static Pages are built by GitLab CI, following the steps
defined in the file [`.gitlab-ci.yml`](.gitlab-ci.yml).

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. Install a Python virtual environment: `python3 -m venv .venv`
1. Enter the virtual environment: `source .venv/bin/activate`
1. Install the dependencies: `pip install -r requirements.txt`
1. Regenerate and serve on port 8000: `make devserver`
1. Visit [http://127.0.0.1:8000](http://127.0.0.1:8000   )

*important to note: [urls are case sensitive](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/343) when deployed but not in local environment. D2LParser works, d2lparser does not.*

Read more at [Pelican's documentation](https://docs.getpelican.com/en/latest/index.html).

### Installing more dev tools

1. Install the dev dependencies: `pip install -r requirements-dev.txt`
1. Install the pre-commit hooks: `pre-commit install`

## Contributing

Contributions are welcome! Please open merge requests against the `master` branch.

### Adding content

There are 3 types of content that can be added to the website:

- **Repositories**: software projects
- **Projects**: bigger projects that can contain multiple repositories and have multiple contributors
- **Contributors**: people or organisations that contribute to the projects

### Adding a page

To add a page, create a new file in the corresponding folder in `content/` and add the following header:
 
```markdown
Title: Title of the page
Date: 2023-09-11 11:00
Tags: tag1, tag2
Slug: slug-of-the-page
Contributors: Contributor1
    Contributor2
```

Contributors are optional, but recommended for repositories and projects. If a project has just one contributor, then that line should look like this, without the final 's':

```
Contributor: Contributor
```

The `Slug` is used to generate the URL of the page. It should be a short version of the title, with words separated by dashes. *Can only be lowercase.*

Check out the existing pages for examples!
